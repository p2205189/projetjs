async function loadimag() {
    try {
        // Récupérer les images depuis l'API Pexels
        let response = await fetch("https://api.pexels.com/v1/search?query=sport", {
            headers: {
                Authorization: '72GHXYutpWQjxU2qeREkuJZ3SgHD6bpSiPeRLJU4l4QMOojWt3GsvkLp',
                Accept: 'application/json'
            }
        });
        // Vérifier si la requête API a réussi
        if (response.ok) {
            // Analyser la réponse JSON
            let data = await response.json();

            // Obtenir le tableau d'images de la réponse
            let images = data.photos;

            // Obtenir l'élément conteneur où les images seront affichées
            let imageContainers = document.getElementsByTagName('aside')[0];
            console.log(imageContainers);
            console.log(document.getElementById("photo"));


            // Vérifier si le nombre d'images récupérées est insuffisant
            if (images.length < 5) {
                throw new Error('Erreur : Pas assez d\'images disponibles.');
            }
            let tabImage = [];
            // Parcourir les 5 premières images
            for (let indiceImage = 0; indiceImage < 5; indiceImage++) {

                // genere un nombre aleatoire
                let random = Math.floor(Math.random() * images.length);
                //genere l'image avec le nombre aleatoire
                let photoData = images[random];
                // si l'image existe deja on recommence la boucle
                if (tabImage.includes(photoData)) {
                    indiceImage--;
                    continue;
                }
                //on ajoute l'image au tableaux d'image
                tabImage.push(photoData);
            }
            for (let image of tabImage) {//ajout des images dans le aside (element qui contien les images)
                //creation d'un nouvel élément de type image
                img = document.createElement("img");
                //initialisation des parametres
                img.src = image.src.original;
                img.setAttribute("width", '100px');
                img.setAttribute("height", '100px');
                img.addEventListener("click", function (e) {
                    suprSelectedImage();
                    e.target.classList.toggle('selected');
                    placerImage();
                });
                //ajout de l'élément de type image à la page html
                imageContainers.insertAdjacentElement('beforeEnd', img);

            }
        }
    }
    catch {

    }

}



function ajoutZone(name) { // permet l'ajout d'une zone dans notre poster
    let newZone = document.createElement('div'); // on créer la zone
    newZone.setAttribute("class", "depot");
    newZone.innerHTML = name;
    newZone.addEventListener("click", function (e) { // on ajoute l'événement lorsque click
        suprSelectedPoster();
        suprSelectedImage()
        e.target.classList.toggle('selected');
    });
    document.getElementById('poster').insertBefore(newZone, null);
}

function suprSelectedPoster() { // permet de suprimer toute les sélections parmis les zones
    let zoneDepot = document.getElementById('poster');
    let selecteds = zoneDepot.getElementsByClassName('selected');
    for (let selected of selecteds) {
        selected.classList.toggle('selected');
    }
}

function suprSelectedImage() { // permet de suprimer toute les sélections parmis les images
    let zoneImage = document.getElementsByTagName('aside')[0];
    let selecteds = zoneImage.getElementsByClassName('selected');
    for (let selected of selecteds) {
        selected.classList.toggle('selected')
    }
}

function placerImage() {
    let zone = document.getElementById('poster').getElementsByClassName('selected')[0]; // récupere la zone sélectioné
    let image = document.getElementsByTagName('aside')[0].getElementsByClassName('selected')[0]; // récupére l'image sélectionné
    let imagePlacer = image.cloneNode(true);
    if (zone != null && zone.getElementsByTagName('img')[0] == null) { // on place l'image si la zone est sélectionner et qu'elle ne contient pas d'image
        image.classList.toggle('selected');
        imagePlacer.classList.toggle('selected');
        zone.innerHTML = '';
        zone.appendChild(imagePlacer);
    } else { // sinon, on gére l'échange de 2 image
        zone2 = document.getElementById('poster').getElementsByTagName('div'); //récupére les zones de dépots
        poster = document.getElementById('poster');
        image = zone //récupére l'image, car la zone sémectionner n'est pas une zone
        console.log(zone);
        for (let div of zone2) {
            console.log(div);
            let tab = div.getElementsByTagName('img');
            if (tab[0] == image) {
                var parent = div; // récupére le parent de l'image (donc une div) pour l'insertion
            }
        }
        if (image != null && poster.getElementsByClassName('selected').length != 0) { //Echange 2 image entre elle
            console.log(image);
            image.remove();
            image.classList.toggle('selected');
            imagePlacer.classList.toggle('selected');
            parent.appendChild(imagePlacer);
            console.log(zone);
        }
    }

}


function main() { //main
    ajoutZone("Zone 1");
    ajoutZone("Zone 2");
    ajoutZone("Zone 3");
    ajoutZone("Zone 4");
    loadimag();
}

main();
